jsdom   = require 'jsdom'
fs      = require 'fs'
jquery  = fs.readFileSync("./jquery-1.11.2.min.js","utf-8")
express = require 'express'

app = express()

app.configure ->
  app.use express.urlencoded()
  app.use express.json()

getGoogleRanking  = (searchPhrase, url, callback)->
  if not searchPhrase? then callback new Error 'empty' else
    query  = searchPhrase.split(' ').join('+')
    result = rank: -1
    start  = 0

    findResults = ->
      console.log "loading:https://www.google.fr/search?q=#{query}&start=#{start}"
      jscrape "https://www.google.fr/search?q=#{query}&start=#{start}", (err,$,response,body)->
        result.top10 = [] if start is 0

        if not err? and $?
          titles = $('h3.r a')
          for i in [0...titles.length]
            answer       = {}
            answer.title = $(titles[i]).text()
            answer.href  = $(titles[i]).attr('href')
            result.top10.push answer if start is 0
            if answer.href? and answer.href.indexOf(url) > -1 and result.rank is -1
              result.rank = start+i+1
              console.log "Trouve en position #{result.rank}"
              callback null, result
          if result.rank is -1 and start < 100
            start += 10
            findResults()
          else
            console.log "pas de resultat"
            callback null, result
        else
          callback err
    findResults()

getFirstWebsite = (term,cb)->
  query = encodeURIComponent term
  url   = "https://www.google.fr/search?q=#{query}&ie=UTF-8&oe=UTF-8"
  blacklist = /wikipedia|societe.com|pagesjaunes|viadeo|linkedin|wiktionary|boursorama|yatedo|booking.com|maps.google|plus.google/;
  console.log url

  jsdom.env
    url:url
    src:[jquery]
    done:(errors,window)->
      $ = window.$
      if errors? then cb new Error "Erreur de Google" else
        titles = $('h3.r a')
        for t in titles
          t = $(t)
          title = t.text()
          website = t.parent().next().find('cite').text()
          if website isnt '' and not website.match(blacklist)
            website = website.match /(https?:\/\/)?(.+)\//
            cb null, title:title,domain:website[2]
            break

getPersonsFromLinkedin = (societe,poste,cb)->
  query = ""
  query += encodeURIComponent(societe + ' ')
  query += encodeURIComponent(poste + ' ') if poste? and poste isnt ''
  query += encodeURIComponent(' intitle:"| Linkedin"')

  profils = []
  getProfils = (start,cb)->
    url = "https://www.google.fr/search?q=#{query}&ie=UTF-8&oe=UTF-8&start=#{start}"
    console.log url
    jsdom.env
      url:url
      src:[jquery]
      done:(errors,window)->
        $ = window.$
        if errors? then cb new Error "Erreur de Google" else
          profils = []
          titles = $('h3.r a')
          if titles.length? and titles.length > 0
            for t in titles
              t = $(t)
              name = t.text().split('|')[0].split(' profils')[0].split(' - ')[0].trim()
              href = t.attr('href')
              job  = t.parent().next().find('.slp').text()
              if href.indexOf('/in/') > -1 or href.indexOf('/pub/') > -1
                profils.push name:name,job:job
          else
            if $('body').html().indexOf('robot')
              profils.push name:'Google OVERDOSE', job:''
            else
              profils.push name:'Google 0 RESULTAT', job:''
          cb null,profils

  getProfils 0, (err,profils)->
    getProfils 10,(err2,profils2)->
      console.log profils
      console.log profils2
      cb null,profils.concat(profils2)

app.get '/get-company-website/',(req,res,next)->
  term = req.query.term
  getFirstWebsite term,(err,website)->
    if err? then res.send 500,err else
      res.send website

app.get '/persons-from-linkedin/',(req,res,next)->
  societe = req.query.societe
  poste   = req.query.poste
  getPersonsFromLinkedin societe,poste,(err,persons)->
    if err? then res.send 500,err else
      res.send persons


app.post '/googlerank/', (req,res,next)->
  console.log req.body
  search = req.body['search']
  url    = req.body['url']
  getGoogleRanking search,url, (err, results)->
    if err?
      res.send 500, err
    else
      res.send results

app.listen(5555)
